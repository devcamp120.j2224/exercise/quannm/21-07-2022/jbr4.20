package com.devcamp.jbr420.jbr420;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {

    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> getListBook() {
        ArrayList<Book> listBook = new ArrayList<Book>();

        Book book1 = new Book("Math", new Author(), 500);
        Book book2 = new Book("JavaScript", new Author("MMM", "mmm@gmail.com", 'M'), 300, 2);
        Book book3 = new Book("Java", new Author(), 400, 5);

        listBook.add(book1);
        listBook.add(book2);
        listBook.add(book3);

        return listBook;
    }
    
    // public static void main(String[] args) {
    //     Book book1 = new Book("Math", new Author(), 500);
    //     Book book2 = new Book("JavaScript", new Author("MMM", "mmm@gmail.com", 'M'), 300, 2);
    //     Book book3 = new Book("Java", new Author(), 400, 5);

    //     System.out.println(book1 + "," + book2 + "," + book3);
    // }
}